<!doctype html>
<html lang="en">n
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/fa/css/fontawesome.min.css">

    <title>About Us</title>
  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
        <a class="navbar-brand" href="#">MENTAL HEALTH CONSULTANT</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
      
        <div class="collapse navbar-collapse" id="navbarColor01">
          <ul class="navbar-nav mr-auto">
        
          </ul>
          <ul class="form-inline navbar-nav">
                <li class="nav-item">
                        <a class="nav-link" href="Index">Home</a>
                     </li>
                <li class="nav-item">
                    <a class="nav-link" href="RegisterPasien">Register</a>
                 </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >Login</a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="LoginPasien">Pasien</a>
                            <a class="dropdown-item" href="LoginKonsultan">Konsultan</a>
                        </div>
                </li>
                <li class="nav-item">
                        <a class="nav-link" href="AboutUs">About Us</a>
                </li>
                
            </ul>
    
        </div>
      </nav>
<br>
<div class="container">
    
        <div class="col-12">
                <div class="card text-white bg-primary ">
                   <center> <h2> About Us </h2></center> 
                </div>
                <p> WEB Mental Health Consultan adalah merupakan sebuah web </p>
        </div>
<br>
        
        <div class="col-12">
                <div class="card text-white bg-primary ">
                   <center> <h2> Owner </h2></center> 
                </div>
        </div>

<br>

        <div class="container-fluid">
             <div class="row">
                    <div class="col-5">
                    <div class="card text-white bg-primary">
                        <center> <h3> CEO </h3></center> 
                    </div>
                    <div>
                            <h3 class="card-header text-center">Putri Nabila</h3>
                            <div class="card-body">
                            </div>
                            <img style="height: 100%; width: 100%; display: block;" src="<?php echo base_url();?>assets/img/pasien1.jpg">
                            <div class="card-body">
                            </div>
                            <ul class="list-group list-group-flush">
                              <li class="list-group-item">Reung-Reung Kec. Kembang Tanjong</li>
                              <li class="list-group-item">putrinabila@gmail.com</li>
                              <li class="list-group-item">085260126004</li>
                            </ul>
                    </div>
                    </div>
                    <div class="col-2 "> 
                         
                    </div>
                    <div class="col-5">
                    <div class="card text-white bg-primary">
                        <center> <h3> CEO </h3></center> 
                    </div>
                    <div>
                            <h3 class="card-header text-center">Rahma Batari</h3>
                            <div class="card-body">
                            </div>
                            <img style="height: 100%; width: 100%; display: block;" src="<?php echo base_url();?>assets/img/batari.jpg">
                            <div class="card-body">
                            </div>
                            <ul class="list-group list-group-flush">
                             
                              <li class="list-group-item">Jl. Dipati Unus, Perumnas, Kota Tangerang </li>
                              <li class="list-group-item">rahmabatari68@gmail.com</li>
                              <li class="list-group-item">081318051478</li>
                            </ul>
                    </div>
                    </div>
            </div>
        </div>


</div>
<br>

<div class="container">
        <div class="card text-white bg-primary ">
           <center> <h2> Contact Us </h2></center> 
        </div>
        <br>

        <div class="row">
                <div class="col-3">
                        <div class="card text-white bg-primary">
                            <center> <h3> Email </h3></center> 
                        </div>
                        <p class="text-center">alamat email</p>
                </div>        
                <div class="col-3">
                    <div class="card text-white bg-primary">
                        <center> <h3> WhatsApp </h3></center> 
                    </div>
                    <p class="text-center">Nomor Wa</p>
                </div>
                <div class="col-3">
                        <div class="card text-white bg-primary">
                            <center> <h3> Facebook </h3></center> 
                        </div>
                        <p class="text-center">nama FB</p>
                </div>        
                <div class="col-3">
                    <div class="card text-white bg-primary">
                        <center> <h3> Instagram </h3></center> 
                    </div>
                    <p class="text-center">nama IG</p>
                </div>

        </div>
</div>
<br>
<div>
<footer class=" card text-white bg-primary text">
    <br>
        <p class="text-center">Copyright @ 2019 - MENTAL HEALTH CONSULTANT</p>
    <br>
    </footer>
</div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="js/jquery-3.3.1.slim.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js" ></script>
  </body>
</html>