<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/fa/css/fontawesome.min.css" />

    <title>Login Pasien</title>
  </head>
  <body>
        <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
                <a class="navbar-brand" href="Index">MENTAL HEALTH CONSULTANT</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>
                </button>
              
                <div class="collapse navbar-collapse" id="navbarColor01">
                  <ul class="navbar-nav mr-auto">
                
                  </ul>
                  <ul class="form-inline navbar-nav">
                        <li class="nav-item">
                                <a class="nav-link">Welcome,  Putri Nabila</a>
                             </li>
               
                        <li class="nav-item">
                                <a class="nav-link" href="Index">Log out</a>
                        </li>
                        
                    </ul>
        
                </div>
              </nav>
<br>
<br>
<br>


<div class="container">
    <div class="row">
        <div class="col-8">
                <table class="table table-hover">
                        <thead>
                          <tr>
                            <th scope="col">No.</th>
                            <th scope="col">Name of Consultant</th>
                            <th scope="col">Contact</th>
                            <th scope="col">Fee</th>
                            <th scope="col">Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php  foreach ($konsultan as $kons) : ?>
                          <tr class="table-active">
                            <th scope="row">1</th>
                            <td> <?=$kons->konsultan_id?></td>
                            <td> <?=$kons->nama?></td>
                            <td> <?=$kons->avail?></td>
                            <td> <a href ="<?=base_url('Home/getConsultant/') . $kons->konsultan_id?>" class="btn btn-primary">Get Consultation</a>
                          </tr>
                          <?php endforeach ?>
                        </tbody>
                      </table> 
        </div>
        <div class="col-4">

                <div class="card mb-3">
                        <h3 class="card-header">Profil Pasien</h3>
                        <div class="card-body">
                        </div>
                        <img style="height: 100%; width: 100%; display: block;" src="<?php echo base_url('assets/').$profile->photo;?>">
                        <div class="card-body">
                        </div>
                        <ul class="list-group list-group-flush">
                          <li class="list-group-item"><?=$profile->nama?></li>
                          <li class="list-group-item"><?=$profile->alamat?></li>
                          <li class="list-group-item"><?=$profile->email?></li>
                          <li class="list-group-item"><?=$profile->hp?></li>
                          <a href = "<?=base_url()?>Home/editProfile" class="btn btn-primary"> Edit Profile </a>
                        </ul>
                </div>       
        </div>
    </div>

</div>

<div>

        <br>
        <br>
        <footer class=" card text-white bg-primary">
        
             <br>
                <p class="text-center">Copyright @ 2019 - MENTAL HEALTH CONSULTANT</p>
            <br>
            </footer>
        </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="<?php echo base_url();?>assets/js/jquery-3.3.1.slim.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/popper.min.js" ></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>
  </body>
</html>