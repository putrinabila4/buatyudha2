<?php 

class M_user extends CI_Model {
    public function find($username){
		$hasil = $this->db->where('username', $username)
						  ->limit(1)
						  ->get('user');
		if($hasil->num_rows() > 0){
			return $hasil->row();
		} else {
			return array();
		}
    }
    public function getKonsultan(){
        $hasil = $this->db->select('*')->from('user')->join('konsultan','user.username = konsultan.username')->where('konsultan.avail','1')->order_by('konsultan.konsultan_id','desc')->get()->result();
        return $hasil;
    }
    public function findKonsultan($id){
        $hasil = $this->db->where('konsultan_id',$id)->limit(1)->get('konsultan')->row();
        return $hasil;
    }
    public function updateUser($username,$data){
        $this->db->where('username',$username)->update('user', $data);
    }
}
